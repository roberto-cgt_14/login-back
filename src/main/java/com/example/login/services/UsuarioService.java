package com.example.login.services;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.example.login.models.Rol;
import com.example.login.models.UsuarioModel;
import com.example.login.repositories.RolRepository;
import com.example.login.repositories.UsuarioRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class UsuarioService {
    @Autowired
    public UsuarioRepository usuarioRepository;
    
    @Autowired
    public RolRepository rolRepository;

    public UsuarioModel guardar(UsuarioModel u) {
    	 u.setRoles(new HashSet<>(rolRepository.findByNombre(u.getRol())));
    
        return usuarioRepository.save(u);
        
    }

    public Optional<UsuarioModel> login(String username, String password) {
        return usuarioRepository.login(username, password);
    }

    public Optional<UsuarioModel> findByUsername(String username) {
        return usuarioRepository.findByUsername(username);
    }
    public List<UsuarioModel> getAllUsers(){
    	return usuarioRepository.findAll();
    }
    public boolean deleteUser(Long id) {
    	try {
			usuarioRepository.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
    }
    
  
}
