package com.example.login.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.login.models.Rol;
import com.example.login.repositories.RolRepository;

@Service
public class RolService {
	
	@Autowired
	private RolRepository repository;
	
	public Rol guardar(Rol rol) {
		return repository.save(rol);
	}
	
	public List<Rol> getAll(){
		return repository.findAll();
	}
	
	

}
