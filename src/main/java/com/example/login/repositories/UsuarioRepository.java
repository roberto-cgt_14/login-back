package com.example.login.repositories;

import java.util.Optional;

import com.example.login.models.UsuarioModel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends JpaRepository<UsuarioModel, Long> {
    @Query(value = "SELECT * FROM usuarios WHERE username= ?1", nativeQuery = true)
    Optional<UsuarioModel> findByUsername(String username);

    @Query(value = "SELECT * FROM usuarios WHERE username= ?1 and password= ?2", nativeQuery = true)
    Optional<UsuarioModel> login(String username, String password);

}