package com.example.login.repositories;



import java.util.HashSet;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.login.models.Rol;



@Repository
public interface RolRepository extends JpaRepository<Rol, Long> {
	
	  /*public Set<Rol> findById(Long id); */
	  public Set<Rol> findByNombre(String nombre);
	

}
