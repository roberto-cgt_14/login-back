package com.example.login.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.login.models.Rol;
import com.example.login.models.UsuarioModel;
import com.example.login.services.RolService;

@RestController
@RequestMapping(value="/roles")
public class RolController {

	@Autowired
	private RolService service;
	
	@GetMapping
	public List<Rol> getAll (){
		return service.getAll();
	}
	
	@PostMapping()
	 public Rol guardar(@RequestBody Rol rol) {
       
        return service.guardar(rol);
    }
	
	
	
	
}
