package com.example.login.controllers;

import java.util.List;
import java.util.Optional;

import com.example.login.config.JasyptEncryptorConfig;
import com.example.login.models.UsuarioModel;
import com.example.login.services.UsuarioService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class UsuarioController {

    @Autowired
    JasyptEncryptorConfig encryp;

    @Autowired
    UsuarioService usuarioService;
    
    @GetMapping("/users")
    public List<UsuarioModel> getAll(){
    	return usuarioService.getAllUsers();
    }

    @PostMapping("/sigin")
    public UsuarioModel guardar(@RequestBody UsuarioModel u) {
        u.setPassword(encryp.passwordEncryptor().encrypt(u.getPassword()).toString());
        return usuarioService.guardar(u);
    }

    @PostMapping("/login")
    public ResponseEntity<Optional<UsuarioModel>> login(@RequestBody UsuarioModel u) {
        Optional<UsuarioModel> usuarioBD = usuarioService.findByUsername(u.getUsername());
      
        if (usuarioBD.isPresent()) {
            String password = usuarioBD.map(UsuarioModel::getPassword).orElse(null);
            String passNoraml = encryp.passwordEncryptor().decrypt(password).toString();
            if (passNoraml.equals(u.getPassword())) {
            	 return ResponseEntity.ok(usuarioBD);
            } else {
            	
            	return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }

        } else {
        	return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
    
    @DeleteMapping("/users/{id}")
    public String eliminarById(@PathVariable("id") Long id) {
    	 boolean ok = this.usuarioService.deleteUser(id);
         if (ok) {
             return "Se eliminó el empleado con id" + id;
         } else {
             return "No se pudo eliminar el empleado con id" + id;
         }
    }
    
    
}
